# GMS Service Example #

An SCXML application to demonstrate how to expose a custom ORS service via Genesys Mobile Services.

### How do I get set up? ###

* Check out the Articles section of the Genesys DevFoundry (https://developer.genesys.com)

### To report issues or ask questions? ###

* Post to the Questions section of the Genesys DevFoundry (https://developer.genesys.com)